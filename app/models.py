from django.db import models

# Create your models here.
class News(models.Model):
    name =models.CharField("news",max_length=250)
    text = models.TextField('news text')


    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "News"
        verbose_name_plural = "Some news"
        
